(function () {
	'use strict';

	angular.module('frontEndApp', [
		'frontEndApp.core',
		'frontEndApp.access',
		'frontEndApp.login',
		'frontEndApp.registration'
	]);

})();
