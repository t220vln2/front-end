(function () {
	'use strict';

	angular
		.module('frontEndApp.registration')
		.controller('Registration', Registration);

	Registration.$inject = ['principal', '$mdDialog', 'logger', '$location'];

	function Registration(principal, $mdDialog, logger, $location) {
		var vm = this;

		vm.register = register;
		vm.hide = $mdDialog.hide;

		function register() {
			logger.info('register called', '', 'Register funciton');
		}

	}

})();
