(function () {
	'use strict';

	angular
		.module('frontEndApp.access')
		.factory('principal', principal);

	principal.$inject = ['$q', '$http', '$localStorage', 'ENV'];

	function principal($q, $http, $localStorage, ENV) {
		var baseUrl;
		var service;

		baseUrl = ENV.apiUrl;

		service = {
			authenticated : authenticated,
      authenticate  : authenticate,
      setToken      : setToken,
      getToken      : getToken,
      identified    : identified,
      identify      : identify,
      setIdentity   : setIdentity,
      getIdentity   : getIdentity,
      forget        : forget
		};

		return service;

		 // Returns if the User is Authenticated
    function authenticated() {
      return angular.isDefined(getToken());
    }

    // Authenticates the User
    function authenticate(username, password) {
      var deferred;
      var request;

      deferred = $q.defer();
      request = {
        method: 'POST',
        url: baseUrl + 'auth/token/',
        headers: {
          'Authorization': 'Basic ' + btoa(username + ":" + password)
        }
      };

      $http(request)
        .then(function (response) {
          $localStorage.token = response.data.token;
          deferred.resolve($localStorage.token);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    // Sets the Access Token of the User
    function setToken(token) {
      $localStorage.token = token;
    }

    // Returns the Access Token of the user
    function getToken() {
      return $localStorage.token;
    }

    // Returns if the User is Identified
    function identified() {
      return angular.isDefined(getIdentity());
    }

    // Identify the User
    function identify() {
      var deferred;

      deferred = $q.defer();

      if (authenticated() && !identified()) {
        $http.get(baseUrl + 'auth/identity/')
          .then(function (response) {
            $localStorage.identity = response.data;
            deferred.resolve($localStorage.identity);
          },
          function (error) {
            deferred.reject(error);
          });
      }
      else {
        deferred.resolve($localStorage.identity);
      }

      return deferred.promise;
    }

    // Returns the Identity of the User
    function getIdentity() {
      return $localStorage.identity;
    }

    // Set the Identity of the User
    function setIdentity(identity) {
      $localStorage.identity = identity;
    }

    // Forget Identity and Authentication
    function forget() {
      setIdentity(undefined);
      setToken(undefined);
    }

	}
})();
