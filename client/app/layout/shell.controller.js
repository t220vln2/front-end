(function () {
	'use strict'; 
	angular
		.module('frontEndApp')
		.controller('ShellCtrl', ShellCtrl);

	ShellCtrl.$inject = ['$mdDialog', 'logger', '$location', 'principal', '$scope'];

	function ShellCtrl($mdDialog, logger, $location, principal, $scope) {

		var vm = this;

		var originatorEv;

		vm.isAuthorized = principal.identified;
		vm.openMenu = openMenu;
		vm.showLoginModal = showLoginModal;
		vm.showRegistrationModal = showRegistrationModal;

		activate();

		function activate() {}

		function openMenu($mdOpenMenu, ev) {
			originatorEv = ev;
			$mdOpenMenu(ev);
		}

		function logout() {
			logger.success('You have been logged out.', '', 'Logout success');
			principal.forget();
			$location.url('/');
		}

		function showLoginModal(ev) {
			$mdDialog.show({
        parent: angular.element(document.body),
        targetEvent: ev,
				scope: $scope,
        clickOutsideToClose:true,
        templateUrl: 'app/login/login.html',
        controller: 'Login',
        preserveScope: true
      });		
		}

		function showRegistrationModal(ev) {
			$mdDialog.show({
        parent: angular.element(document.body),
        targetEvent: ev,
				scope: $scope,
        clickOutsideToClose:true,
        templateUrl: 'app/registration/registration.html',
        controller: 'Registration',
        preserveScope: true
      });		
		}

	}

})();
