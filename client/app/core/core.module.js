(function () {
	'use strict';

	angular.module('frontEndApp.core', [
		'ngCookies',
		'ngResource',
		'ngSanitize',
		'ngAnimate',
		'ngMessages',
		'ngRoute',
		'ngMaterial',
		'ngStorage',

		'blocks.logger'
	]);
})();
