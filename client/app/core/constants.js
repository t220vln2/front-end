(function () {
	'use strict';

	angular
		.module('frontEndApp.core')
		.constant('toastr', toastr)
		.constant('ENV', {
			apiUrl: 'http://localhost:5000/'
		});
})();
