(function () {
	'use strict';

	angular
		.module('blocks.logger')
		.factory('logger', logger);

	logger.$inject = ['$log', 'toastr'];

	function logger($log, toastr) {
		var service;

		service = {
			showToasts: true,

			success : success,
			info    : info,
			warning : warning,
			error   : error,

			// bypass toastr
			log: $log.log
		};

		return service;

		function success(message, data, title) {
			toastr.success(message, title);
			$log.info('Success: ' + message, data);
		}

		function info(message, data, title) {
			toastr.info(message, title);
			$log.info('Info: ' + message, data);
		}

		function warning(message, data, title) {
			toastr.warning(message, title);
			$log.warn('Warning: ' + message, data);
		}

		function error(message, data, title) {
			toastr.error(message, title);
			$log.error('Error: ' + message, data);
		}
	}
})();
