(function () {
	'use strict';

	angular
		.module('frontEndApp.login')
		.controller('Login', Login);

	Login.$inject = ['principal', '$mdDialog', 'logger', '$location'];

	function Login(principal, $mdDialog, logger, $location) {
		var vm = this;

		vm.login = login;
		vm.hide = hide;

		function login() {
			principal.authenticate(vm.username, vm.password)
        .then(authenticationComplete)
        .catch(function (error) {
          rejection('Username or password incorrect, please try again.');
        });


      function authenticationComplete(token) {
        principal.identify()
          .then(identificationComplete)
          .catch(function (error) {
            rejection('Could not Identify user');
          });
      }

      function identificationComplete(identity) {
        logger.success('Welcome ' + vm.username + ', We´ve missed you', 'message', 'Login success!');
        $mdDialog.hide();
        // TODO make it right $location.url('/home');
      }

      function rejection(message) {
        logger.info(message, '', 'Login unsuccesful!');
        vm.password = "";
      }
		}

		function hide() {
			$mdDialog.hide();
		}
	}

})();
